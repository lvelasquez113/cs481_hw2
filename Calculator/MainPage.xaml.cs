﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Calculator
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {
        string UserIn = string.Empty; //stores the user input
        char operation; //used for the operation sign
        double result = 0.0; //keeps track of the result
        string num1 = string.Empty; //keeps numbered entered by the user
        string num2 = string.Empty; //keeps numbered entered by the user

        public MainPage()
        {
            InitializeComponent();
        }

        private void Button_7_Clicked(object sender, EventArgs e)
        {
            calc.Text = " "; //initializes the text to nothing
            UserIn += "7";
            calc.Text += UserIn; //displays number to label box
        }

        private void Button_divide_Clicked(object sender, EventArgs e)
        {
            num1 = UserIn; // stores the user input to num1
            operation = '/'; // stores the operation that will be used
            UserIn = string.Empty; // sets user input to empty for new input
        }

        private void Button_percent_Clicked(object sender, EventArgs e)
        {
            num1 = UserIn; // stores the user input to num1
            operation = '%'; // stores the operation that will be used
            UserIn = string.Empty; // sets user input to empty for new input
        }

        private void Button_clear_Clicked(object sender, EventArgs e)
        {
            calc.Text = " ";
            UserIn = string.Empty;
            num1 = string.Empty;
            num2 = string.Empty;

        }

        private void Button_8_Clicked(object sender, EventArgs e)
        {
            calc.Text = " "; //initializes the text to nothing
            UserIn += "8";
            calc.Text += UserIn; //displays number to label box
        }

        private void Button_9_Clicked(object sender, EventArgs e)
        {
            calc.Text = " "; //initializes the text to nothing
            UserIn += "9";
            calc.Text += UserIn; //displays number to label box
        }

        private void Button_x_Clicked(object sender, EventArgs e)
        {
            num1 = UserIn; // stores the user input to num1
            operation = 'x'; // stores the operation that will be used
            UserIn = string.Empty; // sets user input to empty for new input
        }

        private void Button_4_Clicked(object sender, EventArgs e)
        {
            calc.Text = " "; //initializes the text to nothing
            UserIn += "4";
            calc.Text += UserIn; //displays number to label box
        }

        private void Button_5_Clicked(object sender, EventArgs e)
        {
            calc.Text = " "; //initializes the text to nothing
            UserIn += "5";
            calc.Text += UserIn; //displays number to label box
        }

        private void Button_6_Clicked(object sender, EventArgs e)
        {
            calc.Text = " "; //initializes the text to nothing
            UserIn += "6";
            calc.Text += UserIn; //displays number to label box
        }
        private void Button_minus_Clicked(object sender, EventArgs e)
        {
            num1 = UserIn; // stores the user input to num1
            operation = '-'; // stores the operation that will be used
            UserIn = string.Empty; // sets user input to empty for new input
        }
        private void Button_1_Clicked(object sender, EventArgs e)
        {
            calc.Text = " "; //initializes the text to nothing
            UserIn += "1";
            calc.Text += UserIn; //displays number to label box
        }

        private void Button_2_Clicked(object sender, EventArgs e)
        {
            calc.Text = " "; //initializes the text to nothing
            UserIn += "2";
            calc.Text += UserIn; //displays number to label box
        }

        private void Button_3_Clicked(object sender, EventArgs e)
        {
            calc.Text = " "; //initializes the text to nothing
            UserIn += "3";
            calc.Text += UserIn; //displays number to label box
        }

        private void Button_plus_Clicked(object sender, EventArgs e)
        {
            num1 = UserIn; // stores the user input to num1
            operation = '+'; // stores the operation that will be used
            UserIn = string.Empty; // sets user input to empty for new input
        }

        private void Button_0_Clicked(object sender, EventArgs e)
        {
            calc.Text = " "; //initializes the text to nothing
            UserIn += "0";
            calc.Text += UserIn; //displays number to label box
        }

        private void Button_dot_Clicked(object sender, EventArgs e)
        {
            calc.Text = " "; //initializes the text to nothing
            UserIn += ".";
            calc.Text += UserIn; //displays number to label box
        }

        private void Button_equal_Clicked(object sender, EventArgs e)
        {
            num2 = UserIn;
            double num3;
            double num4;

            double.TryParse(num1, out num3);
            double.TryParse(num2, out num4);

            if(operation == '+')
            {
                result = num3 + num4;
                calc.Text = result.ToString();
            }
            else if(operation == '-')
            {
                result = num3 - num4;
                calc.Text = result.ToString();
            }
            else if(operation == 'x')
            {
                result = num3 * num4;
                calc.Text = result.ToString();
            }
            else if(operation == '/')
            {
                if (num4 == '0')
                    calc.Text = "N/A";
                else
                {
                    result = num3 / num4;
                    calc.Text = result.ToString();
                }
            }
            else if(operation == '%')
            {
                result = num3 / 100;
                calc.Text = result.ToString();
            }
        }
    }
}
